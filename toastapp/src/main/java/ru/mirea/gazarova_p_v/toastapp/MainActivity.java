package ru.mirea.gazarova_p_v.toastapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
         editText = findViewById(R.id.editTextText);
    }

    public void onClickNewActivity(View view) {
        int numbofSymb = editText.getText().length();
        Toast toast = Toast.makeText(getApplicationContext(),
                "Студент №9 Группа БСБО-01-20 Количество символов - " + numbofSymb,
                Toast.LENGTH_SHORT);
        toast.show();
    }
}